module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "jquery": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": 0,
        "linebreak-style": [
            "error",
            "windows"
        ],
        "quotes": 0,
        "semi": [
            "error",
            "always"
        ]
    },
    "globals": {
        "$php_url": false,
        "$site_url": false,
        "$main_url": false,
        "$cookie_name": false,
        "$cookie_path": false
    }
};