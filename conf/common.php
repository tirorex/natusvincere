<?php

date_default_timezone_set('Europe/Moscow');

require_once('config.php');

function die_ajax( $message)
{
    header('HTTP/1.0 404 Not Found');
    echo $message;
    die;
}

function normalizeEmail($email) {
    return strtoupper($email);
}

function generateHash($plain){
    return  password_hash($plain, PASSWORD_DEFAULT);
}

function getPostParameter($name) {
    if (!isset($_POST[$name])) {
        return false;
    } else {
        return htmlspecialchars(stripslashes(trim($_POST[$name])));
    }
}

function getPostArrayParameter($name) {
    if (!isset($_POST[$name])) {
        return false;
    } else {
        return $_POST[$name];
    }
}

function getGetParameter($name) {
    if (!isset($_GET[$name])) {
        return false;
    } else {
        return htmlspecialchars(stripslashes(trim($_GET[$name])));
    }
}

function generateCookie($email){
    return generateHash($email);
}

function getCookie(){
    global $cookie_name;
    if (!isset($_COOKIE[$cookie_name])) {
        return false;
    }

    return $cookie = $_COOKIE[$cookie_name];
}

function checkCookie(){
    global $cookie_name;
    global $user;

    // Если нет куки, возвращаем отказ.
    if (!isset($_COOKIE[$cookie_name])) {
        return false;
    }

    $cookie = $_COOKIE[$cookie_name];

    //Проверяем куки в базе данных. Если она истекла, возвращаем отказ.
    if (!($userID = dbCheckCookie($cookie))){
        clearCookie();
        return false;
    }

    //Если мы тут, куки в порядке.
    $user['player_id'] = $userID;
    return true;
}

function clearCookie(){
    global $cookie_name;
    global $cookie_path;

    setcookie($cookie_name, '',0, $cookie_path);
}

function auth_required(){
    global $site_url;
    header("Location: $site_url");
}

function manual_confirmation_required(){
    global $site_url;
    header("Location: $site_url". "newuser.html");
}

function verifyAccess(){
    global $user;

    if (!checkCookie()){
        auth_required();
    }

    $userData = dbGetUserInfoByCookie(getCookie(), $user['player_id']);
    $user = array_merge($user, $userData);

    if ($user['verified'] == 0){
        manual_confirmation_required();
    }

    if ($user['verified'] == 2){
        manual_confirmation_required();
    }

    //$mainRating = dbGetMainRating();
    //$weeklyRating = dbGetWeeklyRating();
    //$user['mainRating'] = $mainRating[0];
    //$user['mainRatingStartingPoints'] = $mainRating[1];
    //$user['weeklyRating'] = $weeklyRating[0];

    //$activePlayers = dbSelectActivePlayers($mainRating);
    //$user['players'] = $activePlayers;

}

function verifyAdminAccess(){
    global $user;

    verifyAccess();

    if (!$user['isAdmin']){
        auth_required();
    }
}

function verifyPlayerAccess(){
    global $user;

    verifyAccess();

    if (!$user['isPlayer']){
        auth_required();
    }
}

function init(){
    global $user;
    $file_name =  basename($_SERVER['PHP_SELF']);
    if (preg_match('/.html$/', $file_name)) {
        $user['location'] = $file_name ;
    }
}

function getColor($num) {
    if ($num == 1) {
        return 'yellow';
    }
    if ($num == 2) {
        return 'white';
    }

    return 'transparent';
}

function addDuelPoint($duelID){
    if (!($duelResults = dbGetDuelDetails($duelID) )){
        die_ajax('Ошибка при извлечении результата дуэли');
    }

    $date = $duelResults[0]['date'];

    $playerID1 = $duelResults[0]['playerID1'];
    $playerID2 = $duelResults[0]['playerID2'];

    $playerID1ScoreFirstMatch = $duelResults[0]['playerID1Score'];
    $playerID1MissFirstMatch = $duelResults[0]['playerID1Miss'];

    $playerID1ScoreSecondMatch = $duelResults[1]['playerID1Score'];
    $playerID1MissSecondMatch = $duelResults[1]['playerID1Miss'];

	$ratings = dbGetActiveRatings();
    $activeTournament = dbGetActiveTournament();

	foreach ($ratings as $row) {

        $ratingID = $row['rating_type_id'];
        $startingPoints = $row['starting_points'];

        //Если турнир
	    if ($row['status'] == 3){

            $registrationEnd = strtotime($activeTournament['tID']['registration_end']);
            $firstRoundEnd = strtotime($activeTournament['tID']['first_round_end']);
            $now = time();

            //Пропускаем отмененные турниры
            if ($activeTournament['tID']['isCancelled']){
                continue;
            }

            if (dbIsPlayerRegisteredForTournament($playerID1, $ratingID) &&
                dbIsPlayerRegisteredForTournament($playerID2, $ratingID)){
                // Продолжаем;
            } else {
                continue;
            }

            //Заносим результаты только после окончания регистрации турнира
            if ($now < $registrationEnd ){
                continue;
            }

            if  ($activeTournament['tID']['registration_end_confirm'] == 0){
                continue;
            }

            //Заносим результаты, только если турнир еще идет
            if ($now > $firstRoundEnd){
                continue;
            }

            //Если матч уже сыгран
            if (dbIsPairAlreadyPlayedTournament($playerID1, $playerID2, $ratingID)){
                continue;
            }

            //Иначе продолжаем

        }

		$playerPoints = getPlayerPoints($row, $playerID1ScoreFirstMatch, $playerID1MissFirstMatch, $playerID1ScoreSecondMatch, $playerID1MissSecondMatch);

        dbAddDuelPoint( $ratingID, $date, $duelID, $playerID1, $playerPoints['playerID1'], $startingPoints );
        dbAddDuelPoint( $ratingID, $date, $duelID, $playerID2, $playerPoints['playerID2'], $startingPoints );
	}

    return true;

}

function checkDirectAccess(){
    global $isNatusVincere;

    if (!isset($isNatusVincere)){
        die ('Кулхацкер?');
    }
}

function sendMail($email, $subject, $body, $headers){
    mail( $email, "=?utf-8?B?".base64_encode($subject)."?=", htmlspecialchars_decode($body), $headers );
}

function getPlayerPoints($ratingRecord, $playerID1ScoreFirstMatch, $playerID1MissFirstMatch, $playerID1ScoreSecondMatch, $playerID1MissSecondMatch) {

    //Начисляются очки за итог по раундам
    if ($ratingRecord['score_type'] == 1) {
        //Победа первого игрока
        if ( $playerID1ScoreFirstMatch > $playerID1MissFirstMatch && $playerID1ScoreSecondMatch > $playerID1MissSecondMatch ) {
            return array( 'playerID1' => $ratingRecord['win_points'],
                'playerID2'=> $ratingRecord['loose_points'],
                'playerID1Result' => 'win');
        //Победа второго игрока
        } elseif ( $playerID1ScoreFirstMatch < $playerID1MissFirstMatch && $playerID1ScoreSecondMatch < $playerID1MissSecondMatch ) {
            return array( 'playerID1' => $ratingRecord['loose_points'],
                'playerID2'=> $ratingRecord['win_points'],
                'playerID1Result' => 'loose'
                );
        //Ничья
        } else {
            return array( 'playerID1' => $ratingRecord['draw_points'],
                'playerID2'=> $ratingRecord['draw_points'],
                'playerID1Result' => 'draw'
            );
        }
    // Начисление очков по сумме забитых и пропущенных мячей в обоих раундах
    }elseif ($ratingRecord['score_type'] == 2) {
        $playerID1Total = $playerID1ScoreFirstMatch + $playerID1ScoreSecondMatch;
        $playerID2Total = $playerID1MissFirstMatch + $playerID1MissSecondMatch;

        //Победа первого игрока
        if ( $playerID1Total > $playerID2Total ) {
            return array( 'playerID1' => $ratingRecord['win_points'],
                'playerID2'=> $ratingRecord['loose_points'],
                'playerID1Result' => 'win');
        //Победа второго игрока
        } elseif ( $playerID1Total < $playerID2Total ) {
            return array( 'playerID1' => $ratingRecord['loose_points'],
                'playerID2'=> $ratingRecord['win_points'],
                'playerID1Result' => 'loose'
            );
        //Ничья
        } else {
            return array( 'playerID1' => $ratingRecord['draw_points'],
                'playerID2'=> $ratingRecord['draw_points'],
                'playerID1Result' => 'draw'
            );
        }
    }
}

//Поменять игроков местами
function switchPlayers(&$row){
    $tmp = $row['pID1'];
    $row['pID1'] = $row['pID2'];
    $row['pID2'] = $tmp;

    $tmp = $row['pID1Side'];
    $row['pID1Side'] = $row['pID2Side'];
    $row['pID2Side'] = $tmp;

    $tmp = $row['pID1ScoreFirstMatch'];
    $row['pID1ScoreFirstMatch'] = $row['pID1MissFirstMatch'];
    $row['pID1MissFirstMatch'] = $tmp;

    $tmp = $row['pID1ScoreSecondMatch'];
    $row['pID1ScoreSecondMatch'] = $row['pID1MissSecondMatch'];
    $row['pID1MissSecondMatch'] = $tmp;

    $tmp = $row['pID1FIO'];
    $row['pID1FIO'] = $row['pID2FIO'];
    $row['pID2FIO'] = $tmp;

    return true;
}

function normalizeMyResults(&$duels, $tournamentInfo, $userID){
    foreach ($duels as $key => &$row) {
        if ($userID != $row['pID1']) {
            switchPlayers($row);
        }

        $row['pID1Result'] = whoWins($tournamentInfo,
            $row['pID1ScoreFirstMatch'],
            $row['pID1MissFirstMatch'],
            $row['pID1ScoreSecondMatch'],
            $row['pID1MissSecondMatch']);

    }
}

function whoWins($tournamentInfo, $p1FS, $p1FM, $p1SS, $p1SM ){

    $res = getPlayerPoints($tournamentInfo,
        $p1FS,
        $p1FM,
        $p1SS,
        $p1SM
    );

    return $res['playerID1Result'];
}
