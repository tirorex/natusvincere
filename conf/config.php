<?php
/**
 * Created by PhpStorm.
 * User: yk
 * Date: 29.10.2017
 * Time: 17:54
 */

$root_url = 'http://localhost/natusvincere/';

$site_url = $root_url . 'html/';

$main_url = $site_url . 'main.html';

$php_url = $root_url . 'php/';

$img_url =  $root_url . 'img/';

$cookie_path = '/';

$cookie_name = 'natusvincere';

$email_RE = '/^[а-яА-ЯёЁ]+$/iu';

$mysql_encoding = 'utf8';

$DEBUG = true;

$CONFIG['confirmDuel'] = false;

// Prevent direct access php files.
$isNatusVincere = true;