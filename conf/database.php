<?php

$mysqli = mysqli_connect($db_server, $db_username, $db_password, $db_database);

if ($mysqli->connect_errno) {
    die_ajax($mysqli->connect_errno . ' (' . $mysqli->connect_error . ')');
}

if (!$mysqli->set_charset($mysql_encoding)) {
    die_ajax("Ошибка при загрузке набора символов utf8: " . $mysqli->error);
}

function dbSelect()
{
    global $mysqli;

    list ($query, $types, $binds) = func_get_args();

    if (!($stmt = $mysqli->prepare("$query")
    )) {
        die_ajax($mysqli->errno . ' (' . $mysqli->error . ')');
    }

    array_unshift($binds, $types);

    $tmp = array();
    foreach ($binds as $key => $value) {
        $tmp[$key] = &$binds[$key];
    }

    if (!call_user_func_array(array($stmt, "bind_param"), $tmp)) {
        die_ajax($stmt->errno . ' (' . $stmt->error . ')');
    }

    if (!$stmt->execute()) {
        die_ajax($stmt->errno . ' (' . $stmt->error . ')');
    }

    if (!$result = $stmt->get_result()) {
        die_ajax($stmt->errno . ' (' . $stmt->error . ')');
    }

    return $result;
}

function dbDo($query, $types, $binds)
{
    global $mysqli;

    if (!($stmt = $mysqli->prepare("$query")
    )) {
        die_ajax($mysqli->errno . ' (' . $mysqli->error . ')');
    }

    array_unshift($binds, $types);

    $tmp = array();
    foreach ($binds as $key => $value) {
        $tmp[$key] = &$binds[$key];
    }

    if (!call_user_func_array(array($stmt, "bind_param"), $tmp)) {
        die_ajax($stmt->errno . ' (' . $stmt->error . ')');
    }

    if (!$stmt->execute()) {
        die_ajax($stmt->errno . ' (' . $stmt->error . ')');
    }

    return $stmt;
}

function dbInsert()
{

    list ($query, $types, $binds) = func_get_args();

    return dbDo($query, $types, $binds);
}

function dbDelete()
{

    list ($query, $types, $binds) = func_get_args();

    return dbDo($query, $types, $binds);
}

function dbUpdate()
{

    list ($query, $types, $binds) = func_get_args();

    return dbDo($query, $types, $binds);
}

function dbRealSelect($query)
{
    global $mysqli;


    if (!($res = $mysqli->real_query($query))
    ) {
        die_ajax($mysqli->errno . ' (' . $mysqli->error . ')');
    }

    $result = $mysqli->use_result();

    return $result;
}

// Извлекает Email из таблицы ab_email_reg для регистрации новых пользователей по ключу hash
function getEmailByHash($hash)
{
    $query = "SELECT email FROM ab_email_reg a
                            WHERE hash = ?
                            AND a.date >= date_sub(current_timestamp(),  INTERVAL 1 DAY)
                            ORDER BY a.date ASC LIMIT 1";

    $types = 's';
    $binds = array($hash);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

    return $email = $row[0];
}

// Извлекает UserInfo из таблицы abs_players для восстановление пароля
function dbGetUserInfoByEmail($email)
{
    $query = "SELECT player_id, surname FROM abs_players a
                            WHERE email = ?";

    $types = 's';
    $binds = array($email);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

    return array($row[0], $row[1]);
}

//Извлекает ID игрока из таблицы abs_players по ключу email.
function getPlayerIDByEmail($email)
{
    $query = "SELECT player_id FROM abs_players a
                            WHERE email = ?";
    $types = 's';
    $binds = array($email);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

    return $playerID = $row[0];
}

//Занести запись в таблицу ab_lost_password
function dbInsertLostPassword($hash, $userID)
{
    $query = 'INSERT INTO ab_lost_password( hash, userID) VALUES ( ?, ? )';
    $types = 'si';
    $binds = array($hash, $userID);

    $stmt = dbInsert($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

//Извлечь запись из таблицы ab_lost_password
function dbGetLostPassword($hash)
{
    $query = "SELECT userID FROM ab_lost_password
                            WHERE hash = ?
                            and date + INTERVAL 15 MINUTE > CURRENT_TIMESTAMP()";
    $types = 's';
    $binds = array($hash);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

    return $playerID = $row[0];
}

// Обновить пароль
function dbUpdatePassword($userID, $hash)
{
    $query = 'UPDATE abs_players 
                                            SET password = ?
                                                WHERE player_id = ?
                                            ';
    $types = 'si';
    $binds = array($hash, $userID);

    $stmt = dbUpdate($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

//Создание нового пользоватея в табилице abs_players. Привилегии не выдаются.
function createUser($password, $email)
{
    $hash = generateHash($password);
    $query = 'INSERT INTO abs_players( name,
                                                                   surname,
                                                                   middle_name,
                                                                   email,
                                                                   password,
                                                                   verified)
                                                        VALUES (\'-\',\'-\',\'-\', ?, ?, 0)';
    $types = 'ss';
    $binds = array($email, $hash);

    $stmt = dbInsert($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

//Занесение пользователя в таблицу ab_active_users для отлеживания активности пользователей.
function createActiveUser($cookie, $player_id)
{
    $query = 'INSERT INTO ab_active_users( cookie,
                                                                   start_date,
                                                                   last_activity,
                                                                   expiry_date,
                                                                   user_id)
                                                        VALUES (?,
                                                              CURRENT_TIMESTAMP(),
                                                              CURRENT_TIMESTAMP(),
                                                              date_add(current_date(), INTERVAL 1 DAY),
                                                                ? )';
    $types = 'si';
    $binds = array($cookie, $player_id);

    $stmt = dbInsert($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

//Проверяем активность пользователя в таблице ab_active_users.
function dbCheckCookie($cookie)
{
    //global $user;

    $query = "SELECT user_id FROM ab_active_users a
                            WHERE cookie = ?
                            AND start_date <= CURRENT_TIMESTAMP()
                            AND expiry_date > CURRENT_TIMESTAMP()";
    $types = 's';
    $binds = array($cookie);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

//    $user['player_id'] = $row[0];

    return $row[0];
}

//Провердить переданный пароль.
function dbCheckCredentials($email, $password)
{
    $query = "SELECT password FROM abs_players a
                            WHERE email = ?";
    $types = 's';
    $binds = array($email);

    $result = dbSelect($query, $types, $binds);

    if (!($row = $result->fetch_row())) {
        return false;
    }

    $hash = $row[0];

    if (password_verify($password, $hash)) {
        return true;
    } else {
        return false;
    }
}

// Очистить таблицу ab_active_users от неактивных пользователей
function dbDeleteActiveUser($player_id)
{
    $query = "DELETE FROM ab_active_users
                            WHERE user_id = ?";
    $types = 's';
    $binds = array($player_id);

    $stmt = dbDelete($query, $types, $binds);

    return true;
}

//Возвращает данные о пользователи, которые заносятся в глобальную переменную $user
function dbGetUserInfoByCookie($cookie, $playerID)
{

    $query = "SELECT name, surname, middle_name, email, verified
                                              FROM abs_players a
                                              WHERE player_id = ?";
    $types = 's';
    $binds = array($playerID);

    $result = dbSelect($query, $types, $binds);

    //Если пользователя нет в базе, отказ.
    if (!($row = $result->fetch_row())) {
        return false;
    }

    $userData = array();

    $userData['name'] = $row[0];
    $userData['surname'] = $row[1];
    $userData['middle_name'] = $row[2];
    $userData['email'] = $row[3];
    $userData['verified'] = $row[4];

    $query = "SELECT p.priv_id, p.priv_name, up.user_id
                                                FROM ab_privs p, ab_user_privs up
                                                WHERE p.priv_id = up.priv_id
                                                      AND up.user_id = ?";
    $types = 'i';
    $binds = array($playerID);

    $result = dbSelect($query, $types, $binds);

    $userData['isAdmin'] = false;
    $userData['isPlayer'] = false;
    $userData['isObserver'] = false;

    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        if ($row[0] == 1) {
            $userData['isAdmin'] = true;
        }

        if ($row[0] == 2) {
            $userData['isPlayer'] = true;
        }

        if ($row[0] == 3) {
            $userData['isObserver'] = true;
        }
    }

    return $userData;
}

//Обновление основной информации о пользователях в таблице abs_players
function dbUpdateUserInfo($player_id, $surname, $name, $middle_name)
{
    $query = 'UPDATE abs_players 
                                            SET surname = ?,
                                                name = ?,
                                                middle_name = ?,
                                                verified = 1
                                                WHERE player_id = ?
                                            ';
    $types = 'sssi';
    $binds = array($surname, $name, $middle_name, $player_id);

    $stmt = dbUpdate($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

//Получение списка запросов на регистрацию
function dbRequests()
{
    $query = "SELECT player_id, surname, name, middle_name, email
                                              FROM abs_players a
                                              WHERE verified = 2";
    $result = dbRealSelect($query);

    if (!$result) {
        return false;
    }

    $requests = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($requests, [$row[0], $row[1], $row[2], $row[3], $row[4]]);
    }

    return $requests;
}

//Обновить поле verified в таблице abs_players
function dbChangeUserVerified($state, $user_ids)
{
    $query = 'UPDATE abs_players 
                                            SET verified = ?
                                                WHERE player_id = ?
                                            ';
    $types = 'ii';

    for ($i = 0; $i < count($user_ids); $i++) {

        $id = $user_ids[$i];
        if (!$id) {
            continue;
        }

        $binds = array($state, $id);
        $stmt = dbUpdate($query, $types, $binds);

        if ($stmt->affected_rows != 1) {
            return false;
        }
    }

    return true;
}

//Добавление новости в таблицу ab_news
function dbInsertNews($player_id, $header, $text)
{
    $query = 'INSERT INTO ab_news( user_id,
                                                                   news_header,
                                                                   news)
                                                        VALUES (?,
                                                                ?,
                                                                ?)';
    $types = 'iss';
    $binds = array($player_id, $header, $text);

    $stmt = dbInsert($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    } else {
        return true;
    }
}

function dbSelectModels($limit = 999999){
    $query = "SELECT eq_model_id, eq_brand_id, eq_type_id, eq_model_name
                                                FROM eq_models
                                                ORDER BY eq_model_id 
                                                LIMIT ?";
    $types = 'i';
    $binds = array($limit);

    $result = dbSelect($query, $types, $binds);

    $eq_models = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($eq_models, [$row[0], $row[1], $row[2], $row[3] ]);
    }
    return $eq_models;
}


function dbSelectVendors($limit = 999999){
    $query = "SELECT eq_brand_id, eq_brand_name
                                                FROM eq_vendors
                                                ORDER BY eq_brand_id DESC
                                                LIMIT ?";
    $types = 'i';
    $binds = array($limit);

    $result = dbSelect($query, $types, $binds);

    $eq_vendors = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($eq_vendors, [$row[0], $row[1] ]);
    }
    return $eq_vendors;
}

function dbSelectEquipmentSubTypes($limit = 999999){
    $query = "SELECT eq_sub_id, eq_type_id, eq_sub_name
                                                FROM eq_subtypes
                                                ORDER BY eq_sub_id DESC
                                                LIMIT ?";
    $types = 'i';
    $binds = array($limit);

    $result = dbSelect($query, $types, $binds);

    $eq_subtypes = array();
//    while ($row = $result->fetch_array(MYSQLI_NUM)) {
//        array_push($eq_subtypes, [$row[0], $row[1] ]);
//    }

    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        if (!isset($eq_subtypes[$row[1]])) {
            $eq_subtypes[$row[1]] = array();
        }
        array_push($eq_subtypes[$row[1]], [$row[0], $row[2]]);
    }

    return $eq_subtypes;
}


function dbSelectEquipmentTypes($limit = 999999){
    $query = "SELECT eq_id, eq_type
                                                FROM eq_types
                                                ORDER BY eq_id DESC
                                                LIMIT ?";
    $types = 'i';
    $binds = array($limit);

    $result = dbSelect($query, $types, $binds);

    $eq_types = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($eq_types, [$row[0], $row[1] ]);
    }

    return $eq_types;
}

//Получение новостей
function dbSelectNews($limit = 30)
{
    $query = "SELECT date, news_header, news
                                                FROM ab_news
                                                ORDER BY date DESC
                                                LIMIT ?";
    $types = 'i';
    $binds = array($limit);

    $result = dbSelect($query, $types, $binds);

    $news = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($news, [$row[0], $row[1], $row[2]]);
    }

    return $news;
}

//Добавление пользовательских привилегий в таблицу ab_user_privs
function dbInsertUserPrivs($priv, $user_ids)
{
    $query = 'INSERT INTO ab_user_privs
                                                       (user_id, priv_id )
                                                  VALUES (?, ?)';
    $types = 'ii';

    for ($i = 0; $i < count($user_ids); $i++) {

        $id = $user_ids[$i];
        if (!$id) {
            continue;
        }

        $binds = array($id, $priv);
        $stmt = dbInsert($query, $types, $binds);

        if ($stmt->affected_rows != 1) {
            return false;
        }
    }

    return true;
}

function dbAddNewEq($eq_type, $eq_day, $eq_month, $eq_year, $eq_serial, $eq_comments, $eq_subtype, $eq_vendor, $eq_model ) {
    global $mysqli;

    $query = "INSERT INTO eq_equipment( eq_type, eq_day, eq_month, eq_year, eq_serial, eq_comments, eq_subtype, eq_vendor, eq_model) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )";

    $types = 'iiiissiii';
    $binds = array($eq_type, $eq_day, $eq_month, $eq_year, $eq_serial, $eq_comments, $eq_subtype, $eq_vendor, $eq_model );

    $stmt = dbInsert($query, $types, $binds);

    if ($stmt->affected_rows != 1) {
        return false;
    }

    return $mysqli->insert_id;

}

function dbSelectEqInfo($eq_id)
{
    $query = "select e.eq_id, t.eq_type, e.eq_day, e.eq_month, e.eq_year,
       e.eq_serial, st.eq_sub_name, v.eq_brand_name, m.eq_model_name
       from eq_equipment e,
                    eq_types t,
                    eq_subtypes st,
                    eq_vendors v,
                    eq_models m
        where e.eq_type = t.eq_id and
    e.eq_subtype = st.eq_sub_id and
    e.eq_vendor = v.eq_brand_id and
    e.eq_model = m.eq_model_id and e.eq_id = ?";

    $types = 'i';
    $binds = array($eq_id);

    $result = dbSelect($query, $types, $binds);

    $row = $result->fetch_row();

    return $row;
}

function dbSelectEqOp($eq_id)
{
    $query = "select e.eq_id, t.eq_type, e.eq_day, e.eq_month, e.eq_year,
       e.eq_serial, st.eq_sub_name, v.eq_brand_name, m.eq_model_name
       from eq_equipment e,
                    eq_types t,
                    eq_subtypes st,
                    eq_vendors v,
                    eq_models m
        where e.eq_type = t.eq_id and
    e.eq_subtype = st.eq_sub_id and
    e.eq_vendor = v.eq_brand_id and
    e.eq_model = m.eq_model_id and e.eq_id = ?";

    $types = 'i';
    $binds = array($eq_id);

    $result = dbSelect($query, $types, $binds);

    $eq_op = array();
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        array_push($eq_op, [$row[0], $row[1], $row[2]]);
    }



    return $eq_op;
}


function dbListEquipments(){
    $query = "select e.eq_id, t.eq_type, e.eq_day, e.eq_month, e.eq_year,
       e.eq_serial, st.eq_sub_name, v.eq_brand_name, m.eq_model_name
       from eq_equipment e,
                    eq_types t,
                    eq_subtypes st,
                    eq_vendors v,
                    eq_models m
        where e.eq_type = t.eq_id and
    e.eq_subtype = st.eq_sub_id and
    e.eq_vendor = v.eq_brand_id and
    e.eq_model = m.eq_model_id";

  //  $query = 'select * from eq_equipment;';

    $result = dbRealSelect($query);

    if (!$result) {
        return false;
    }

    $eq_list = array();
    while ($row = $result->fetch_row()) {
        $eq_list[$row[0]] = [ $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7],$row[8]
        ];
    }

    return $eq_list;
}

