create table ab_tournament_registration
(
  tr_ID int auto_increment
    primary key,
  pID int not null,
  tID int not null
)
;

create table ab_tournament_info
(
  tiID int auto_increment
    primary key,
  tID int not null,
  tType int not null,
  registration_end datetime not null,
  registration_end_confirm int default '0' not null,
  first_round_end datetime not null,
  first_round_end_confirm int default '0' not null
)
;

