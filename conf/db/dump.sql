-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: 127.0.0.1
-- ------------------------------------------------------
-- Server version	10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_active_users`
--

DROP TABLE IF EXISTS `ab_active_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_active_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie` varchar(60) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_active_users`
--

LOCK TABLES `ab_active_users` WRITE;
/*!40000 ALTER TABLE `ab_active_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_active_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_duel_results`
--

DROP TABLE IF EXISTS `ab_duel_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_duel_results` (
  `duel_resuld_id` int(11) NOT NULL AUTO_INCREMENT,
  `duel_id` int(11) NOT NULL,
  `playerID1` int(11) NOT NULL,
  `playerID2` int(11) NOT NULL,
  `playerID1Side` int(11) NOT NULL,
  `playerID2Side` int(11) NOT NULL,
  `playerID1Score` int(11) NOT NULL,
  `playerID2Score` int(11) NOT NULL,
  PRIMARY KEY (`duel_resuld_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_duel_results`
--

LOCK TABLES `ab_duel_results` WRITE;
/*!40000 ALTER TABLE `ab_duel_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_duel_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_duels`
--

DROP TABLE IF EXISTS `ab_duels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_duels` (
  `duel_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `playerID1` int(11) NOT NULL,
  `playerID2` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`duel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_duels`
--

LOCK TABLES `ab_duels` WRITE;
/*!40000 ALTER TABLE `ab_duels` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_duels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_email_reg`
--

DROP TABLE IF EXISTS `ab_email_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_email_reg` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(60) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `ab_email_reg_email_index` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_email_reg`
--

LOCK TABLES `ab_email_reg` WRITE;
/*!40000 ALTER TABLE `ab_email_reg` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_email_reg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_news`
--

DROP TABLE IF EXISTS `ab_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `news_header` varchar(200) NOT NULL,
  `news` text NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_news`
--

LOCK TABLES `ab_news` WRITE;
/*!40000 ALTER TABLE `ab_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_privs`
--

DROP TABLE IF EXISTS `ab_privs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_privs` (
  `priv_id` int(11) NOT NULL AUTO_INCREMENT,
  `priv_name` varchar(200) NOT NULL,
  `priv_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`priv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_privs`
--

LOCK TABLES `ab_privs` WRITE;
/*!40000 ALTER TABLE `ab_privs` DISABLE KEYS */;
INSERT INTO `ab_privs` VALUES (1,'Admin','Администратор портала'),(2,'Player','Игрок'),(3,'Observer','Наблюдатель'),(4,'Manager','');
/*!40000 ALTER TABLE `ab_privs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating`
--

DROP TABLE IF EXISTS `ab_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  `rating_type` int(11) NOT NULL,
  `user_state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating`
--

LOCK TABLES `ab_rating` WRITE;
/*!40000 ALTER TABLE `ab_rating` DISABLE KEYS */;
INSERT INTO `ab_rating` VALUES (1,1,1000,1,1);
/*!40000 ALTER TABLE `ab_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating_changes`
--

DROP TABLE IF EXISTS `ab_rating_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating_changes` (
  `rating_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_type` int(11) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `duel_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `rating_change` int(11) NOT NULL,
  `rating_total` int(11) NOT NULL,
  PRIMARY KEY (`rating_history_id`),
  KEY `ab_rating_changes_player_id_index` (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating_changes`
--

LOCK TABLES `ab_rating_changes` WRITE;
/*!40000 ALTER TABLE `ab_rating_changes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_rating_changes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating_types`
--

DROP TABLE IF EXISTS `ab_rating_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating_types` (
  `rating_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_name` varchar(200) NOT NULL,
  `rating_desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`rating_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating_types`
--

LOCK TABLES `ab_rating_types` WRITE;
/*!40000 ALTER TABLE `ab_rating_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_rating_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_user_privs`
--

DROP TABLE IF EXISTS `ab_user_privs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_user_privs` (
  `user_priv_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `priv_id` int(11) NOT NULL,
  PRIMARY KEY (`user_priv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_user_privs`
--

LOCK TABLES `ab_user_privs` WRITE;
/*!40000 ALTER TABLE `ab_user_privs` DISABLE KEYS */;
INSERT INTO `ab_user_privs` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `ab_user_privs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abs_players`
--

DROP TABLE IF EXISTS `abs_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abs_players` (
  `player_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `verified` int(11) NOT NULL,
  `vk_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abs_players`
--

LOCK TABLES `abs_players` WRITE;
/*!40000 ALTER TABLE `abs_players` DISABLE KEYS */;
INSERT INTO `abs_players` VALUES (1,'Юрий','Кардюков','Александрович','ADS@ART-VECTOR.COM','$2y$10$dWU.9bTDbsutnN7th6rzIeSw8OHy1pTeHPpwBB0Q0Nra8LULxTiQy',1,NULL);
/*!40000 ALTER TABLE `abs_players` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-02 18:06:10
