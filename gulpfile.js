var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minify_css = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    concat  = require('gulp-concat'),
    path = require('path'),
    uglify = require('gulp-uglify');

gulp.task('sass', function () {
    //gulp.src("src/bootstrap/scss/**/*.scss")
    gulp.src('src/bootstrap/custom_scss/**/custom.scss')
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                includePaths: [],
                imagePath: "path/to/images"
            })
                .on('error', sass.logError))

        // https://github.com/ai/browserslist
       // .pipe(autoprefixer("last 4 version", "> 1%", "Explorer >= 8", {
        //    cascade: true
        //}))

       // .pipe(minify_css({compatibility: 'ie8'}))
      //  .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/css"));
});

gulp.task('styles', function(){
    return gulp.src('wp-content/plugins/ddota2ru_common/assets/css/*.css')
        .pipe(concat('site.css'))
        .pipe(minify_css())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest('wp-content/plugins/ddota2ru_common/assets/css/minify'))
});

gulp.task('sass-admin', function () {
    //gulp.src("src/bootstrap/scss/**/*.scss")
    gulp.src('src/bootstrap/custom_scss/**/admin-custom.scss')
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                includePaths: [],
                imagePath: "path/to/images"
            })
                .on('error', sass.logError))

        // https://github.com/ai/browserslist
        .pipe(autoprefixer("last 4 version", "> 1%", "Explorer >= 8", {
            cascade: true
        }))

        // .pipe(minify_css({compatibility: 'ie8'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/css"))
        .pipe(gulp.dest("wp-content/themes/ddota2ru/assets/css"));
});

gulp.task('js-plugin-user', function () {
    //gulp.src("src/bootstrap/scss/**/*.scss")
    gulp.src(['src/js/plugin/*.js', 'src/js/plugin/user/*.js'])
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('ddota2_plugin_user.min.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/js"))
        .pipe(gulp.dest("wp-content/plugins/ddota2ru_common/assets/js"));
});

gulp.task('js-plugin-admin', function () {
    //gulp.src("src/bootstrap/scss/**/*.scss")
    gulp.src(['src/js/plugin/*.js', 'src/js/plugin/admin/*.js'])
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('ddota2_plugin_admin.min.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/js"))
        .pipe(gulp.dest("wp-content/plugins/ddota2ru_common/assets/js/admin"));
});


gulp.task('js-theme', function () {
    //gulp.src("src/bootstrap/scss/**/*.scss")
    gulp.src('src/js/theme/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('ddota2_theme.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/js"))
        .pipe(gulp.dest("wp-content/themes/ddota2ru/assets/js"));
});

