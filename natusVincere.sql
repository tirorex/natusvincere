-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: natusvincere
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `natusvincere`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `natusvincere` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `natusvincere`;

--
-- Table structure for table `ab_active_users`
--

DROP TABLE IF EXISTS `ab_active_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_active_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie` varchar(60) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_active_users`
--

LOCK TABLES `ab_active_users` WRITE;
/*!40000 ALTER TABLE `ab_active_users` DISABLE KEYS */;
INSERT INTO `ab_active_users` VALUES (1,'$2y$10$Wy5PJSxX4tSrn9ps2WxlMOfoSUqnn3YdK.WQppIPz7BccDSAzI7OW','2019-06-07 11:29:28','2019-06-07 21:00:00','2019-06-07 11:29:28',2),(2,'$2y$10$NSyVADqza2hEfRe9T6mY2.Rc3qj49VN4tDh4mhOOn.Nhkj62dZjSy','2019-06-07 11:42:20','2019-06-07 21:00:00','2019-06-07 11:42:20',3),(3,'$2y$10$5K3uOfUJHnzdkhS4HtV7FeRoCMsMBOSQBL7OF3bcQbBbxGJeG7OnW','2019-06-08 06:45:43','2019-06-08 21:00:00','2019-06-08 06:45:43',2),(4,'$2y$10$ZWPfbjg7JTuWPu/GsZFwrOZtv9xk9zbJm4XXC46xuNKgT2PH36Ch.','2019-06-08 06:49:14','2019-06-08 21:00:00','2019-06-08 06:49:14',3),(5,'$2y$10$vHo3NiRfhLwsHiCnal9lseXPLsMzX2lOo5VDtoM3fgy8PjlGjnJcO','2019-06-08 07:30:53','2019-06-08 21:00:00','2019-06-08 07:30:53',4),(6,'$2y$10$oS..WFqxCX9xwapxQCuI5uFe6i4XlsI2Oa5n1PMOlAm8N3/22zl9K','2019-06-08 09:00:58','2019-06-08 21:00:00','2019-06-08 09:00:58',5),(7,'$2y$10$d/q.yrssrjm5LhWD/a4LFekzIWqt0o9Wdwo3LprP4b5l43LhC5v3O','2019-06-08 09:30:13','2019-06-08 21:00:00','2019-06-08 09:30:13',6),(8,'$2y$10$MvLQq5mXdF1mF2YsMbF4O.bfptUjJyJ/.HMB.f7Nb3FZvmYLsjPmC','2019-06-08 13:28:53','2019-06-08 21:00:00','2019-06-08 13:28:53',7);
/*!40000 ALTER TABLE `ab_active_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_duel_results`
--

DROP TABLE IF EXISTS `ab_duel_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_duel_results` (
  `duel_resuld_id` int(11) NOT NULL AUTO_INCREMENT,
  `duel_id` int(11) NOT NULL,
  `playerID1` int(11) NOT NULL,
  `playerID2` int(11) NOT NULL,
  `playerID1Side` int(11) NOT NULL,
  `playerID2Side` int(11) NOT NULL,
  `playerID1Score` int(11) NOT NULL,
  `playerID2Score` int(11) NOT NULL,
  PRIMARY KEY (`duel_resuld_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_duel_results`
--

LOCK TABLES `ab_duel_results` WRITE;
/*!40000 ALTER TABLE `ab_duel_results` DISABLE KEYS */;
INSERT INTO `ab_duel_results` VALUES (1,1,6,0,0,0,0,0),(2,1,6,0,0,0,0,0),(3,2,6,0,0,0,0,0),(4,2,6,0,0,0,0,0),(5,3,6,0,0,0,0,0),(6,3,6,0,0,0,0,0),(7,4,6,0,0,0,0,0),(8,4,6,0,0,0,0,0),(9,5,6,0,0,0,0,0),(10,5,6,0,0,0,0,0);
/*!40000 ALTER TABLE `ab_duel_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_duels`
--

DROP TABLE IF EXISTS `ab_duels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_duels` (
  `duel_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `playerID1` int(11) NOT NULL,
  `playerID2` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`duel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_duels`
--

LOCK TABLES `ab_duels` WRITE;
/*!40000 ALTER TABLE `ab_duels` DISABLE KEYS */;
INSERT INTO `ab_duels` VALUES (1,'2019-06-08 14:38:54',6,0,1),(2,'2019-06-08 14:41:03',6,0,1),(3,'2019-06-08 14:41:28',6,0,1),(4,'2019-06-08 14:43:02',6,0,1),(5,'2019-06-08 14:43:37',6,0,1);
/*!40000 ALTER TABLE `ab_duels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_email_reg`
--

DROP TABLE IF EXISTS `ab_email_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_email_reg` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(60) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `ab_email_reg_email_index` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_email_reg`
--

LOCK TABLES `ab_email_reg` WRITE;
/*!40000 ALTER TABLE `ab_email_reg` DISABLE KEYS */;
INSERT INTO `ab_email_reg` VALUES (4,'2019-06-07 12:27:32','CCC@SFS.RU','$2y$10$YqQLW/s5/58wmYE8kPMfSeQ1a9ktwppMTbBAMWfilkyNOYfu11Nxe',0),(5,'2019-06-07 18:23:21','S@S.RU','$2y$10$AJ2/hJM2rRJ00GJHHQLviOgQ9zQ9l3BW/zcjx/ljeUu0yU0n4R5Si',0),(6,'2019-06-08 06:45:11','DD@DD.RU','$2y$10$lKiQRw1CNLNzq.C2w9rP2.YAis3Z068qQkdC4vyF/o.A6T9slyMIu',0),(7,'2019-06-08 06:48:47','II@II.RU','$2y$10$dN1ccOheuMvYq7ct6692G.LVoxOOpFoibPT2YOzLiF65rSEPgIs9q',0),(8,'2019-06-08 07:30:28','JJ@JJ.RU','$2y$10$U/8dSG.cVslVgzTnBXur9Oul948qXpVh5RdPFQunAzSbMrPm8ofeG',0),(9,'2019-06-08 09:00:41','CC@CC.RU','$2y$10$u3PW3G6uBXp4h.gBOHDaweGpJB41A74/mEW//0btuVxIr.L./ynIC',0),(10,'2019-06-08 09:30:01','OO@OO.RU','$2y$10$qixm3h2t.puo3aMRpoTGTOGnGjroPf22EkUjIzmaxQuD1/5ZdH3J6',0),(11,'2019-06-08 13:28:18','PP@PP.RU','$2y$10$5qiHV5drpnUFfhdPipI0quPpIs6t0RD/kfzo1pfDokbIH.SVVBeMy',0);
/*!40000 ALTER TABLE `ab_email_reg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_news`
--

DROP TABLE IF EXISTS `ab_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `news_header` varchar(200) NOT NULL,
  `news` text NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_news`
--

LOCK TABLES `ab_news` WRITE;
/*!40000 ALTER TABLE `ab_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_privs`
--

DROP TABLE IF EXISTS `ab_privs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_privs` (
  `priv_id` int(11) NOT NULL AUTO_INCREMENT,
  `priv_name` varchar(200) NOT NULL,
  `priv_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`priv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_privs`
--

LOCK TABLES `ab_privs` WRITE;
/*!40000 ALTER TABLE `ab_privs` DISABLE KEYS */;
INSERT INTO `ab_privs` VALUES (1,'Admin','Администратор портала'),(2,'Player','Игрок'),(3,'Observer','Наблюдатель'),(4,'Manager','');
/*!40000 ALTER TABLE `ab_privs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating`
--

DROP TABLE IF EXISTS `ab_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  `rating_type` int(11) NOT NULL,
  `user_state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating`
--

LOCK TABLES `ab_rating` WRITE;
/*!40000 ALTER TABLE `ab_rating` DISABLE KEYS */;
INSERT INTO `ab_rating` VALUES (1,1,1000,1,1);
/*!40000 ALTER TABLE `ab_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating_changes`
--

DROP TABLE IF EXISTS `ab_rating_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating_changes` (
  `rating_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_type` int(11) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `duel_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `rating_change` int(11) NOT NULL,
  `rating_total` int(11) NOT NULL,
  PRIMARY KEY (`rating_history_id`),
  KEY `ab_rating_changes_player_id_index` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating_changes`
--

LOCK TABLES `ab_rating_changes` WRITE;
/*!40000 ALTER TABLE `ab_rating_changes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_rating_changes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_rating_types`
--

DROP TABLE IF EXISTS `ab_rating_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_rating_types` (
  `rating_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_name` varchar(200) NOT NULL,
  `rating_desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`rating_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_rating_types`
--

LOCK TABLES `ab_rating_types` WRITE;
/*!40000 ALTER TABLE `ab_rating_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_rating_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_tournament_info`
--

DROP TABLE IF EXISTS `ab_tournament_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_tournament_info` (
  `tiID` int(11) NOT NULL AUTO_INCREMENT,
  `tID` int(11) NOT NULL,
  `tType` int(11) NOT NULL,
  `registration_end` datetime NOT NULL,
  `registration_end_confirm` int(11) NOT NULL DEFAULT '0',
  `first_round_end` datetime NOT NULL,
  `first_round_end_confirm` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tiID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_tournament_info`
--

LOCK TABLES `ab_tournament_info` WRITE;
/*!40000 ALTER TABLE `ab_tournament_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_tournament_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_tournament_registration`
--

DROP TABLE IF EXISTS `ab_tournament_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_tournament_registration` (
  `tr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `pID` int(11) NOT NULL,
  `tID` int(11) NOT NULL,
  PRIMARY KEY (`tr_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_tournament_registration`
--

LOCK TABLES `ab_tournament_registration` WRITE;
/*!40000 ALTER TABLE `ab_tournament_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `ab_tournament_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ab_user_privs`
--

DROP TABLE IF EXISTS `ab_user_privs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab_user_privs` (
  `user_priv_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `priv_id` int(11) NOT NULL,
  PRIMARY KEY (`user_priv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ab_user_privs`
--

LOCK TABLES `ab_user_privs` WRITE;
/*!40000 ALTER TABLE `ab_user_privs` DISABLE KEYS */;
INSERT INTO `ab_user_privs` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `ab_user_privs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `abs_players`
--

DROP TABLE IF EXISTS `abs_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abs_players` (
  `player_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `verified` int(11) NOT NULL,
  `vk_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abs_players`
--

LOCK TABLES `abs_players` WRITE;
/*!40000 ALTER TABLE `abs_players` DISABLE KEYS */;
INSERT INTO `abs_players` VALUES (1,'Юрий','Кардюков','Александрович','ADS@ART-VECTOR.COM','$2y$10$dWU.9bTDbsutnN7th6rzIeSw8OHy1pTeHPpwBB0Q0Nra8LULxTiQy',1,NULL),(2,'Петрович','Петр','Петровович','DD@DD.RU','$2y$10$FAqPrwstfhZ1J2FtmeYU.uL86DXtH9scGWby9/SNsXtbiRhQCCvz.',1,NULL),(3,'Петровичпп','Петрв','ппп','II@II.RU','$2y$10$mowm8f49N5i/XwKrXmjw4.00FnpfmcGk25XMezGd6bJHvp7hnsm2C',1,NULL),(4,'Петр','Иванов','Петрович','JJ@JJ.RU','$2y$10$YWDCCql/sDcw3t3ZcldEhuBXxU0XM0j0i5/pERMuFpdi3pnguU/qW',1,NULL),(5,'Петр','Иванов','Петрович','CC@CC.RU','$2y$10$SePwpd4EhGrYGz6XnJBdgOno7Xe13TxK3Br3dQJdPGC0h/RVR9RES',1,NULL),(6,'Петр','Иванов','Петрович','OO@OO.RU','$2y$10$uG2GB0swPFmkHbVJhJAf.OqpnMQt359iAAw44Lr3o56lhnIcHdIv6',1,NULL),(7,'Петр','Иванов','Петрович','PP@PP.RU','$2y$10$N8aHqu6EDuf1/Yo61wOGcOdLTTxriJMHzIupNb9jJLXSPvium.tca',1,NULL);
/*!40000 ALTER TABLE `abs_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'St.Petersburg'),(2,'Moscow');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `c_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `course_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_available_actions`
--

DROP TABLE IF EXISTS `eq_available_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_available_actions` (
  `eq_aa_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_aa_name` varchar(200) NOT NULL,
  PRIMARY KEY (`eq_aa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_available_actions`
--

LOCK TABLES `eq_available_actions` WRITE;
/*!40000 ALTER TABLE `eq_available_actions` DISABLE KEYS */;
INSERT INTO `eq_available_actions` VALUES (1,'Поставлено на учет'),(2,'Произведен ремонт'),(3,'Смена владельца'),(4,'События отказов');
/*!40000 ALTER TABLE `eq_available_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_equipment`
--

DROP TABLE IF EXISTS `eq_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_equipment` (
  `eq_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_type` int(11) NOT NULL,
  `eq_day` int(11) DEFAULT NULL,
  `eq_serial` varchar(200) NOT NULL,
  `eq_comments` varchar(200) DEFAULT NULL,
  `eq_month` int(11) DEFAULT NULL,
  `eq_year` int(11) DEFAULT NULL,
  `eq_subtype` int(11) NOT NULL,
  `eq_vendor` int(11) NOT NULL,
  `eq_model` int(11) NOT NULL,
  PRIMARY KEY (`eq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_equipment`
--

LOCK TABLES `eq_equipment` WRITE;
/*!40000 ALTER TABLE `eq_equipment` DISABLE KEYS */;
INSERT INTO `eq_equipment` VALUES (12,2,1,'1111','111111     ',1,1961,4,2,4),(13,1,2,'22222','22222',2,1962,3,1,2),(14,2,6,'44','            55erte',4,1963,3,1,1);
/*!40000 ALTER TABLE `eq_equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_models`
--

DROP TABLE IF EXISTS `eq_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_models` (
  `eq_model_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_brand_id` int(11) NOT NULL,
  `eq_type_id` int(11) NOT NULL,
  `eq_model_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`eq_model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_models`
--

LOCK TABLES `eq_models` WRITE;
/*!40000 ALTER TABLE `eq_models` DISABLE KEYS */;
INSERT INTO `eq_models` VALUES (1,1,2,'GE Proteus XR/a'),(2,1,2,'GE Definium 8000'),(3,1,2,'GE Definium 5000'),(4,2,2,'Philips DigitalDiagnost'),(5,3,2,'Siemens Harmony 10T'),(6,3,2,'Siemens Symphony 15T');
/*!40000 ALTER TABLE `eq_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_subtypes`
--

DROP TABLE IF EXISTS `eq_subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_subtypes` (
  `eq_sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_type_id` int(11) NOT NULL,
  `eq_sub_name` varchar(200) NOT NULL,
  PRIMARY KEY (`eq_sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_subtypes`
--

LOCK TABLES `eq_subtypes` WRITE;
/*!40000 ALTER TABLE `eq_subtypes` DISABLE KEYS */;
INSERT INTO `eq_subtypes` VALUES (1,2,'Стационарные ренгены'),(2,2,'Мобильные рентгены'),(3,2,'Портативные'),(4,2,'Терапевтические');
/*!40000 ALTER TABLE `eq_subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_supply`
--

DROP TABLE IF EXISTS `eq_supply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_supply` (
  `eq_sup_id` int(11) DEFAULT NULL,
  `eq_sup_name` varchar(200) DEFAULT NULL,
  `eq_sup_address` varchar(200) DEFAULT NULL,
  `eq_sup_phone` varchar(200) NOT NULL,
  `eq_sup_email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_supply`
--

LOCK TABLES `eq_supply` WRITE;
/*!40000 ALTER TABLE `eq_supply` DISABLE KEYS */;
INSERT INTO `eq_supply` VALUES (1,'ООО Медприбор','Санкт-Петербург','+7 (812) 336-40-36','info@medpipspb.ru'),(2,'ООО Дельрус','Москва','+7 (495) 120-77-00','moscow@delrus.ru'),(3,'АО Фармадис','Москва','+7 (495) 641-75-55','info@pharmadys.ru');
/*!40000 ALTER TABLE `eq_supply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_types`
--

DROP TABLE IF EXISTS `eq_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_types` (
  `eq_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`eq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_types`
--

LOCK TABLES `eq_types` WRITE;
/*!40000 ALTER TABLE `eq_types` DISABLE KEYS */;
INSERT INTO `eq_types` VALUES (1,'МРТ'),(2,'Рентген'),(3,'Вентиляция легких');
/*!40000 ALTER TABLE `eq_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_vendors`
--

DROP TABLE IF EXISTS `eq_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_vendors` (
  `eq_brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_brand_name` varchar(200) NOT NULL,
  PRIMARY KEY (`eq_brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_vendors`
--

LOCK TABLES `eq_vendors` WRITE;
/*!40000 ALTER TABLE `eq_vendors` DISABLE KEYS */;
INSERT INTO `eq_vendors` VALUES (1,'General Electric'),(2,'Philips'),(3,'Siemens');
/*!40000 ALTER TABLE `eq_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `universities`
--

DROP TABLE IF EXISTS `universities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `universities` (
  `u_id` int(11) NOT NULL,
  `u_name` varchar(200) NOT NULL,
  `city` int(11) DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `universities`
--

LOCK TABLES `universities` WRITE;
/*!40000 ALTER TABLE `universities` DISABLE KEYS */;
/*!40000 ALTER TABLE `universities` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-08 18:59:54
