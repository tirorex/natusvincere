<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 16:56
 */

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');

verifyAdminAccess();

$data = getPostArrayParameter('data');

$program = $data[0];

if ($program == 'changeUserVerified') {

    $flag = $data[1];
    $user_ids = $data[2];
    $priv = $data[3];

    if (!dbChangeUserVerified($flag, $user_ids)) {
        die_ajax('Хуйня');
    }

    if ($flag == 1 ) {
        if (!dbInsertUserPrivs($priv, $user_ids)) {
            die_ajax('Большая хуйня');
        }

        if ($priv == 2) {

            for ($i = 0; $i < count($user_ids); $i++) {
                $id = $user_ids[$i];

                if (!$id) {
                    continue;
                }

                dbInsertRating($id, $user['mainRating'], $user['mainRatingStartingPoints']);
            }
        }
    }

    echo "Успешно";
}

if ($program == 'addNews') {
    $header = htmlspecialchars($data[1]);
    $news = htmlspecialchars($data[2]);
    $sendMail = htmlspecialchars($data[3]);

    if (strlen($header) < 10 || strlen($news) < 10) {
        die_ajax('Введите корректный заголовок и тело новости');
    }

    if (dbInsertNews($user['player_id'], $header, $news)){

    } else {
        die_ajax('Хуйня' . $news);
    }

    if ($sendMail) {

        $subject = 'Новости портала ABSoccer.ru';
        $active_players = dbSelectActivePlayers($user['mainRating']);
        foreach ($active_players as $row){
            $headers = 'From: ABSoccer <no-reply@absoccer.ru>' . "\r\n" .
                "Content-type: text/html; charset=utf-8\r\n";
            $email = $row[3];
            if ($DEBUG) {
                $email = 'ads@art-vector.com';
            }
            sendMail($email, $subject, $news, $headers);
            if ($DEBUG) {
                break;
            }


        }

    }
}

if ($program == 'manageTournaments') {

    $name = $data[1];
    $desc = $data[2];
    $start = $data[3];
    $end = $data[4];
    $type = $data[5];
    $points = $data[6];
    $wdl = $data[7];
    $status = $data[8];
    $tournamentID = $data[9];

    list ($win, $draw, $loose) = explode('/', $wdl);

    if ($tournamentID){
        if (dbUpdateTournament($tournamentID, $name, $desc, $start, $end, $type, $points, $win, $draw, $loose, $status)){
            return 'Все отлично';
        }
    } else {
        if (dbInsertTournament($name, $desc, $start, $end, $type, $points, $win, $draw, $loose, $status)) {
            return 'Все отлично';
        }
    }
}
if ($program == 'deleteTournaments') {

    $tournamentID = $data[1];

    if (dbDeleteTournament($tournamentID)) {
        return 'Все отлично';
    }
}

die_ajax(print_r($data));