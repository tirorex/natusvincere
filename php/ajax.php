<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 16:55
 */

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');

verifyPlayerAccess();

$data = getPostArrayParameter('data');

$program = $data[0];

if ($program == 'addNewEq'){
    global $user;
    global $DEBUG;
    global $CONFIG;

    $eq_type = $data[1];

    $eq_day = $data[2];
    $eq_month = $data[3];
    $eq_year = $data[4];

    $eq_serial = $data[5];
    $eq_comments = $data[6];

    $eq_subtype = $data[7];
    $eq_vendor = $data[8];
    $eq_model = $data[9];


    $new_eq_id = dbAddNewEq($eq_type, $eq_day, $eq_month, $eq_year, $eq_serial, $eq_comments, $eq_subtype, $eq_vendor, $eq_model );

    echo "Оборудование успешно добавлено в реестр";
}


if ($program == 'addResults') {
    global $user;
    global $DEBUG;
    global $CONFIG;

    $side1 = $data[1];
    $side2 = $data[2];
    $villain = $data[3];
    $side1_score = $data[4];
    $side1_miss = $data[5];
    $side2_score = $data[6];
    $side2_miss = $data[7];

    if ( $CONFIG['confirmDuel']) {
    	$duelStatus = 0;
    } else {
	    $duelStatus = 1;
    }

	if (!$DEBUG) {
		if ( !dbGetDuelPair( $user['player_id'], $villain ) ) {
			die_ajax( 'Учитывается один рейтинговый поединок c одним соперником в сутки.' );
		}
	}

    if (!($duelID = dbAddDuel($user['player_id'], $villain, $duelStatus))){
         die_ajax('Возникла ошибка.');
    }

    $firstMatch = dbAddDuelResults($duelID, $user['player_id'], $villain, $side1, $side2, $side1_score, $side1_miss);
    $secondMatch = dbAddDuelResults($duelID, $user['player_id'], $villain, $side2, $side1, $side2_score, $side2_miss);

    if (!$CONFIG['confirmDuel']) {
	    if (!addDuelPoint($duelID)) {
		    die_ajax('Ошибка при начислении очков рейтинга.');
	    }
    }

    if (!$firstMatch && !$secondMatch){
            die_ajax('Возникла ошибка.');
        }
    echo "Результат успешно занесен в реестр";
}

if ($program == 'confirmResults') {
    global $user;
	global $CONFIG;
	if (!$CONFIG['confirmDuel']) {
		die_ajax('Отключено подтверждение результатов матчей.');
	}

    $decision = $data[1];
    $duelID = $data[2];

    if (!dbConfirmResults($user['player_id'], $decision, $duelID)){
        die_ajax('Ошибка');
    }

    if ($decision == 1) {
        if (!addDuelPoint($duelID)) {
            die_ajax('Ошибка при начислении очков рейтинга.');
        }
    }

    echo "Спасибо!";
}

if ($program == 'getMyResults') {
    global $user;

    $tID = $data[1];

    $allTournaments = dbAllRatings();
    $duels = dbGetMyResults($user['player_id'], $tID );
    normalizeMyResults($duels, $allTournaments[ $tID ], $user['player_id']);

   // krsort($duels, SORT_NUMERIC );

    echo json_encode($duels);
}

if ($program == 'tournamentRegistration') {
    global $user;

    $pID = $data[1];
    $tID = $data[2];

    if ($user['player_id'] != $pID){
        die_ajax('Кулхацкер?');
    }

    if(!dbRegisterTournament($pID, $tID)){
        die_ajax('Ошибка при занесении в реестры');
    }

    echo 'Вы успешно зарегистировались в турнире';
}

