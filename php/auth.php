<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 24.10.2017
 * Time: 15:37
 */

require_once('../conf/db.php');
require_once('../conf/common.php');

$email = getPostParameter('email');
$email = normalizeEmail($email);
$password = getPostParameter('password');

if (!dbCheckCredentials($email, $password)){
    die_ajax('Неверные данные');
}

$cookie = generateCookie($email);
$id = getPlayerIDByEmail($email);

createActiveUser($cookie, $id);

echo $cookie;



//