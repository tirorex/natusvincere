<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 24.10.2017
 * Time: 14:34
 */

require_once('../conf/bootstrap.php');

//if (!isset($_POST['path'])){
//    die_ajax('Email не зарегистрирован.');
//}

if (isset($_POST['hash'])){
    $hash = getPostParameter('hash');
    $password1 = getPostParameter('password1');
    $password2 = getPostParameter('password2');

    if (!($userID = dbGetLostPassword($hash))){
        die_ajax('Нет записей');
    }


    $dbHash = generateHash($password1);

    if (!dbUpdatePassword($userID, $dbHash)){
        die_ajax('Ошибка смены пароля');
    }

    echo 'Пароль успешно сменен';
    exit;

}

if (!isset($_POST['email']) || !isset($_POST['surname'])){
    die_ajax('Поля не заполнены');
}



$email = getPostParameter('email');
$surname = getPostParameter('surname');

list( $playerID, $dbSurname) = dbGetUserInfoByEmail(normalizeEmail($email));

if (mb_strtoupper($dbSurname) != mb_strtoupper($surname)){
    die_ajax('Данные не найдены в реестре.');
}

$hash = generateHash($email . $surname);
if (!dbInsertLostPassword($hash, $playerID)){
    die_ajax('Ошибка занесения в реестр.');
}

if ($DEBUG){
    echo ". $site_url" . "lostpassword.html?path=".urlencode($hash);
} else {

    $subject = 'Восстановление пароля ABSoccer.ru';
    $message = "Вы получили это письмо, потому что подали заявку на восстановление пароля
<br><br>Для продолжения регистрации проследуйте по ссылке
 <a href='" . $site_url . "lostpassword.html?path=" . urlencode( $hash ) . "'>Восстановление пароля</a>.
 " . "<br><br>" . "С уважением, администрация портала ABSoccer.ru.";

    $headers = 'From: ABSoccer <no-reply@absoccer.ru>' . "\r\n" .
        "Content-type: text/html; charset=utf-8\r\n";

    sendMail($email, $subject, $message, $headers);
    echo "На ваш email отправлено письмо.";
}