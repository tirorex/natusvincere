<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 11:42
 */

if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');

// Friendly name; Link; State, Badge

function generateMainMenu()
{
    global $user;
    global $site_url;

    $common = array(
        ['База данных', $site_url . 'eqList.html', 1, 0],
        ['Новости', $site_url . 'news.html', 1, 0],
        ['Черный список', $site_url . 'black.html', 1, 0],
        ['Профиль', $site_url . 'profile.html', 0, 0]

    );
    if ($user['isAdmin']) {
        array_push($common, ['Добавить оборудование', $site_url . 'add_new_equipment.html', 1, 0]);
        array_push($common, ['Добавить операцию с оборудованием', $site_url . 'add_op_equipment.html', 1, 0]);
        array_push($common, ['Администратор', $site_url . 'admin.html', 1, 0]);
    }


//	if ($user['isPlayer']) {
//		array_push($common, ['Мои игры', $site_url . 'mygames.html', 1, 0]);
//	}


	//array_push($common, ['Профиль', $site_url . 'profile.html', 1, 0]);

    return $common;
}

function renderMainMenu()
{
    global $user;
    $menu = generateMainMenu();

    ?>
    <div class="col-3 col-lg-2 px-0" >
        <ul class="list-group" >
    <?php
    $re = "/$user[location]$/";

    foreach ($menu as $value){

        if (preg_match($re, $value[1])) {
            $active = 'active';
            $class = 'btn-primary';
        } else {
            $active = '';
            $class = 'btn-light';
        }

        if ($value[2] == 0) {
            $disabled = 'disabled';
        } else {
            $disabled = '';
        }

        echo "<li class='list-group-item p-0'><a class='btn $class btn-block py-2 pl-3 text-left $active $disabled' href='" .$value[1]."'>". $value[0] ."</a></li >";
    }
    ?>
            </ul >
        </div >
    <?php
}