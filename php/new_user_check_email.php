<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 24.10.2017
 * Time: 14:34
 */

require_once('../conf/db.php');
require_once('../conf/common.php');

if (!isset($_POST['path'])){
    die_ajax('Email не зарегистрирован.');
}

if (!isset($_POST['password1']) || !isset($_POST['password2'])){
    die_ajax('Пароли не заданы.');
}

$password1 = getPostParameter('password1');
$password2 = getPostParameter('password2');

if ($password1 != $password2) {
    die_ajax('Пароли не совпадают.');
}

if (mb_strlen($password1) < 6 || mb_strlen($password1) > 50) {
    die_ajax('Некорректная длина пароля');
}

$hash = getPostParameter('path');
$email = getEmailByHash($hash);

if (getPlayerIDByEmail($email)){
    die_ajax('Email уже зарегистрирован.');
}

if (!createUser($password1, $email)){
    die_ajax('Что-то пошло не так.');
}

$playerID = getPlayerIDByEmail($email);

$cookie = generateCookie($email);


if (!createActiveUser($cookie, $playerID)){
    die_ajax('Не могу начать сессию.');
}

echo $cookie;