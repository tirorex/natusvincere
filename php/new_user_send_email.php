<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 24.10.2017
 * Time: 14:34
 */

require_once('../conf/db.php');
require_once('../conf/common.php');

if (!isset($_POST['email'])){
    die_ajax('Введите email');
}

$email = getPostParameter('email');
$email = normalizeEmail($email);
$hash = generateHash($email);

// Check for email duplicate
if (getPlayerIDByEmail($email)){
    die_ajax('Email уже зарегистрирован.');
}

if (!($stmt = $mysqli->prepare("SELECT count(a.date) from ab_email_reg a
                                        where email = upper(?)
                                        and a.date >= date_sub(current_timestamp(),  INTERVAL 1 MINUTE)"
                            )
    )) {
    die_ajax($mysqli->connect_errno . ' (' . $mysqli->connect_error. ')');

}

if (!$stmt->bind_param('s', $email)){
    die_ajax($stmt->connect_errno . ' (' . $stmt->connect_error. ')');
}

if (!$stmt->execute()){
    die_ajax($stmt->connect_errno . ' (' . $stmt->connect_error. ')');
}

if (!$result = $stmt->get_result()){
    die_ajax($stmt->connect_errno . ' (' . $stmt->connect_error. ')');
}

$row = $result->fetch_row();
$count = $row[0];

if ($count != 0) {
    die_ajax('Слишком частая регистрация. Повторите через одну минуту.');
}

$stmt->close();

if (!($stmt = $mysqli->prepare(
                'INSERT INTO ab_email_reg(email, hash) values (?,?)'
                )
     )
){
    die_ajax($mysqli->connect_errno . ' (' . $mysqli->connect_error. ')');
}

if (!$stmt->bind_param('ss', $email, $hash)
){
    die_ajax($stmt->connect_errno . ' (' . $stmt->connect_error. ')');
}

if (!$stmt->execute()){
    die_ajax($stmt->connect_errno . ' (' . $stmt->connect_error. ')');
}

$stmt->close();

if ($DEBUG){
	echo ". $site_url" . "createpassword.html?path=".urlencode($hash);
} else {

    $subject = 'Регистрация на портале Natus Vincere';
	$message = "Вы получили это письмо, потому что подали заявку на регистрацию на портале 
ABSoccer.ru<br><br>Если вы не подавали заявку на регистрацию, проигнорируте данное письмо
<br><br>Для продолжения регистрации проследуйте по ссылке
 <a href='" . $site_url . "createpassword.html?path=" . urlencode( $hash ) . "'>Подтверждение e-mail</a>.
 " . "<br><br>" . "С уважением, администрация портала ABSoccer.ru.";

	$headers = 'From: ABSoccer <no-reply@absoccer.ru>' . "\r\n" .
	           "Content-type: text/html; charset=utf-8\r\n";

    sendMail($email, $subject, $message, $headers);
	echo ".";
}
