<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 27.10.2017
 * Time: 16:22
 */

require_once('../conf/db.php');
require_once('../conf/common.php');

if (!checkCookie()){
    auth_required();
}

$userData = dbGetUserInfoByCookie(getCookie(), $user['player_id']);
$user = array_merge($user, $userData);

//dbGetUserInfoByCookie(getCookie(), $user['player_id']);

if ($user['verified'] == 2 ){
    die_ajax('Пожалуйста, дождитесь подтверждения регистрации.');
}


$name = getPostParameter('name');
$surname = getPostParameter('surname');
$middle_name = getPostParameter('middle_name');

if (!preg_match($email_RE, $surname) ||
    !preg_match($email_RE, $name) ||
    !preg_match($email_RE, $middle_name)
){
    die_ajax('Пожалуйста, только кириллица');
}


if (!dbUpdateUserInfo($user['player_id'], $surname, $name, $middle_name)){
    die_ajax($user['player_id'] . $user['email']. " $surname, $name, $middle_name");
}else {
    echo 'Все в порядке';
}