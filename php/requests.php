<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 14:37
 */

if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');

function renderRequests()
{
    $requests = dbRequests();

    ?>
    <table class="table table-striped table-bordered table-hover table-sm table-responsive-lg">
    <thead class="thead-dark">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Фамилия</th>
        <th scope="col">Имя</th>
        <th scope="col">Отчество</th>
        <th scope="col">Email</th>
        <th scope="col"><i class="fa fa-gavel" aria-hidden="true"></i></th>
    </tr>
    </thead>
    <tbody>
    <?php
            foreach ($requests as $value){
                echo '<tr>';
                echo "<th>$value[0]</th>";
                echo "<td>$value[1]</td>";
                echo "<td>$value[2]</td>";
                echo "<td>$value[3]</td>";
                echo "<td>$value[4]</td>";
                echo "<td>
                    <div class='form-check'>
                        <label class='form-check-label'>
                            <input type='checkbox' name='$value[0]' class='form-check-input'>
                        </label>    
                    </div>
                </td>";
                echo '</tr>';
            }
            ?>
        </tbody>
      </table>
    <?php
}
