<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 11:42
 */
if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');


function renderAddOpEqupiment()
{
    global $user;
    $eq_types = dbSelectEquipmentTypes();
    $eq_subtypes = dbSelectEquipmentSubTypes();
    $eq_vendors = dbSelectVendors();
    $eq_models = dbSelectModels();

    echo '<div class="container">';
    ?>
    <form id='nv_add_new_eq'  class="mt-1 pt-1">
<?php
    _renderEqType($eq_types);
    _renderEqSubType($eq_subtypes[2], 2);
    _renderVendors($eq_vendors);
    _renderModels($eq_models);
    _renderStartDate();
    _renderSerial();
    _renderAddInfo();
    ?>
        <div class="row">
            <div class="col text-right">
                <a class="btn btn-primary" href="#" id="nv-add-new-eq">Внести оборудование</a>
            </div>

        </div>
    </form>
    <?php
    echo '</div>';
}

function _renderVendors($eq_vendors){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Производитель
        </div>
        <div class="col-sm">
            <?php

            echo '<select class="form-control" name="eq_vendor" id="eq_vendor">';

            foreach ($eq_vendors as $value){
                echo "<option value='".$value[0]."'>". htmlspecialchars_decode($value[1]). "</option>";

            }
            echo     '</select>';

            ?>
        </div>
    </div>
    <?php
}

function _renderModels($eq_models){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Модель
        </div>
        <div class="col-sm">
            <?php

            echo '<select class="form-control" name="eq_model" id="eq_model">';
            foreach ($eq_models as $value){
                echo "<option value='".$value[1]."'>". htmlspecialchars_decode($value[3]). "</option>";

            }
            echo     '</select>';

            ?>
        </div>
    </div>
    <?php
}

function _renderEqSubType($eq_subtypes){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Категория оборудования
        </div>
        <div class="col-sm">
            <?php

            echo '<select class="form-control" name="eq_subtype" id="eq_subtype">';

            foreach ($eq_subtypes as $value){
                echo "<option value='".$value[0]."'>". htmlspecialchars_decode($value[1]). "</option>";

            }
            echo     '</select>';

            ?>
        </div>
    </div>


    <?php
}


function _renderEqType($eq_types){
    ?>
        <div class="row py-1">
            <div class="col-sm">
                Тип оборудования
            </div>
            <div class="col-sm">
                <?php

                echo '<select class="form-control" name="eq_type" id="eq_type">';

                foreach ($eq_types as $value){
                    echo "<option value='".$value[0]."'>". htmlspecialchars_decode($value[1]). "</option>";

                }
                echo     '</select>';

                ?>
            </div>
        </div>


    <?php
}

function _renderStartDate(){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Дата ввода в эксплуатацию
        </div>
        <div class="col-sm">
            День
            <?php

            echo '<select class="form-control" id="nv_day" name="nv_day">';
                for ($i = 1; $i <=31; $i++) {
                    echo "<option value='" .$i. "'>".$i."</option>";
                }

            echo     '</select>';

            ?>
        </div>
        <div class="col-sm">
            Месяц
            <?php

            echo '<select class="form-control" id="nv_month" name="nv_month">';
            echo "<option value='1'>Январь</option>";
            echo "<option value='2'>Февраль</option>";
            echo "<option value='3'>Март</option>";
            echo "<option value='4'>Апрель</option>";
            echo "<option value='5'>Май</option>";
            echo "<option value='6'>Июнь</option>";
            echo "<option value='7'>Июль</option>";
            echo "<option value='8'>Август</option>";
            echo "<option value='9'>Сентябрь</option>";
            echo "<option value='10'>Октябрь</option>";
            echo "<option value='11'>Ноябрь</option>";
            echo "<option value='12'>Декабрь</option>";
            echo     '</select>';

            ?>
        </div>
        <div class="col-sm">
            Год
            <?php

            echo '<select class="form-control" id="nv_year" name="nv_year">';
            for ($i = 1960; $i <=2019; $i++) {
                echo "<option value='" .$i. "'>".$i."</option>";
            }

            echo     '</select>';

            ?>
        </div>
    </div>


    <?php
}


function _renderSerial(){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Серийный номер
        </div>
        <div class="col-sm">
            <input type="text" class="form-control" placeholder="Серийный номер"  id="nv_serial" name="nv_serial">
        </div>
    </div>


    <?php
}

function _renderAddInfo(){
    ?>
    <div class="row py-1">
        <div class="col-sm">
            Дополнительная информация
        </div>
        <div class="col-sm">
            <textarea type="text" class="form-control" id="nv_add_info" name="nv_add_info">
            </textarea>
        </div>
    </div>


    <?php
}
