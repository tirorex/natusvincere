<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 11:42
 */
if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');


function renderEqCard($eq_id)
{
    global $user;

    $eq_info = dbSelectEqInfo($eq_id);
    $eq_op = dbSelectEqOp($eq_id);

    echo '<div class="h4 mt-3 pt-3">GE Definium 8000 X-ray System</div>';
echo '<img height="200px" src="../img/GE.jpeg">';
    echo '<table class="table table-striped table-bordered table-hover ">';
    echo '<thead>';
    echo '<tr class="bg-primary">';
    echo '<th scope="col">Параметр</th>';
    echo '<th scope="col">Значение</th>';
    echo '</tr>';
    echo '</thead>';

    echo '<tr>';
    echo '<th scope="row">Тип</th>';
    echo '<td class="" >МРТ</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">Класс</th>';
    echo '<td class="" >Стационарные ренгены</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">Поставщик</th>';
    echo '<td class="" >General Electric</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">Serial</th>';
    echo '<td class="" >123552-456</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">Год выпуска</th>';
    echo '<td class="" >2009</td>';
    echo '</tr>';

    echo "</table>";

    echo '<div class="h4">История оборудования</div>';

    echo '<table class="table table-striped table-bordered table-hover ">';
    echo '<thead>';
    echo '<tr class="">';
    echo '<th scope="col">Дата</th>';
    echo '<th scope="col">Событие</th>';
    echo '<th scope="col">Подрядчик</th>';
    echo '</tr>';
    echo '</thead>';

    echo '<tr>';
    echo '<th scope="row">12.03.2017</th>';
    echo '<td class="" >Плановое ТО</td>';
    echo '<td class="" >АО Аргус</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">15.04.2015</th>';
    echo '<td class="" >Ремонт, замена редуктора</td>';
    echo '<td class="" >ООО РемСервис</td>';
    echo '</tr>';

    echo '<tr>';
    echo '<th scope="row">15.04.2009</th>';
    echo '<td class="" >Введение в эксплуатацию</td>';
    echo '<td class="" >ЗАО Геомед</td>';
    echo '</tr>';


    echo '</table>';

    echo '<p style="height: 100px;"></p>';
}
