<?php

if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');

function renderListEquipment()
{
    global $user;
    //print_r($user);exit;
    $listEq = dbListEquipments();

    $eq_types = dbSelectEquipmentTypes();
    $eq_subtypes = dbSelectEquipmentSubTypes();
    $eq_vendors = dbSelectVendors();
    $eq_models = dbSelectModels();
    _renderEqType($eq_types);
    _renderEqSubType($eq_subtypes[2], 2);
    _renderVendors($eq_vendors);
    _renderModels($eq_models);

    ?>
    <div class="row">
            <div class="col text-right">
                <a class="btn btn-primary" href="#" id="nv-search">Поиск</a>
            </div>

        </div>
    <?php
    echo '<div class="h4 mt-3 pt-3">Список медицинского оборудования</div>';
   // renderSelectTournament($allTournaments);
    echo '<table id="res" class="table table-striped table-bordered table-hover table-sm">';
    echo '<thead>';
    echo '<tr class="bg-primary">';
    echo '<th scope="col">ID</th>';
    echo '<th scope="col">Тип</th>';
    echo '<th scope="col">Класс</th>';
    echo '<th scope="col">Поставщик</th>';
    echo '<th scope="col">Модель</th>';
    echo '<th scope="col">Serial</th>';
    echo '<th scope="col">Дата</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    $count = 1;
    foreach ($listEq as $key => $row){
        _renderListItem($key,$row);
    }
    echo '</tbody>';
    echo '</table>';
}

function _renderListItem($eqID, $row){
    global $user;
    global $site_url;

    //Победа
//    if ($result == 'win'){
//        $res_color = 'green';
//        $res_text = 'Победа';
//    //Поражение
//    } elseif ($result == 'loose'){
//        $res_color = 'red';
//        $res_text = 'Поражение';
//
//    //Ничья
//    } elseif ($result) {
//        $res_color = 'white';
//        $res_text = 'Ничья';
//    } else {
//        $res_color = 'transparent';
//        $res_text = 'Не сыгран';
//    }

    echo '<tr>';
    echo '<th scope="row"><a href="'. $site_url . 'showEqCard.html?eqid='. $eqID.'">'.$eqID.'</a></th>';
    echo '<td class="text-center" >'.$row[0] .'</td>';
    echo '<td style="white-space:nowrap;">'.$row[5].'</td>';
    echo '<td class="text-center" >'.$row[6] .'</td>';
    echo '<td class="text-center" >'.$row[7] .'</td>';
    echo '<td class="text-center" >'.$row[4] .'</td>';
    echo '<td class="text-center" >'.$row[3] .'</td>';
    echo '</tr>';
}