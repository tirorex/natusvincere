<?php
/**
 * Created by PhpStorm.
 * User: kardyukovyua
 * Date: 30.10.2017
 * Time: 11:42
 */
if (!isset($isNatusVincere)){
    die ('Кулхацкер?');
}
require_once('../conf/bootstrap.php');


function renderNews()
{
    global $user;
    $news = dbSelectNews();
    foreach ($news as $value){
        ?>
        <div class="row">
            <div class="col">
                <div class="fontStyle my-2 ml-3">
                    <div class="card">
                        <div class="card-body">
                    <?php
                    echo "<h4 class='card-title'>$value[1]</h4>";
                    echo "<h6 class='card-subtitle mb-2 text-muted'>$value[0]</h6>";
                    echo "<p class='card-text'>" .htmlspecialchars_decode($value[2]) . "</p>";
                    ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}