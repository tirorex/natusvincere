
(function ($) {

    $(function () {



        $('#nv-add-new-eq').on('click', function (event) {

            event.preventDefault();

            // let side1 = $('input[name="side1"]:checked').val();
            // let side2 = $('input[name="side2"]:checked').val();

            let eq_type = $('select[name="eq_type"]').val();
            let nv_day = $('select[name="nv_day"]').val();
            let nv_month = $('select[name="nv_month"]').val();
            let nv_year = $('select[name="nv_year"]').val();

            let eq_subtype = $('select[name="eq_subtype"]').val();
            let eq_vendor = $('select[name="eq_vendor"]').val();
            let eq_model = $('select[name="eq_model"]').val();

            let nv_serial = $('#nv_serial').val();
            let nv_add_info = $('#nv_add_info').val();

            let $error = '';


            let data = [];
            data[0] = 'addNewEq';
            data[1] = eq_type;
            data[2] = nv_day;
            data[3] = nv_month;
            data[4] = nv_year;
            data[5] = nv_serial;
            data[6] = nv_add_info;
            data[7] = eq_subtype;
            data[8] = eq_vendor;
            data[9] = eq_model;

            if (!nv_serial) {
                $error += 'Необходимо заполнить серийный номер оборудования<br>';
            }


            if ($error) {
                $('#anchor').addClass('bg-danger').html('<br>' + $error + '<br>');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: $php_url + 'ajax.php',
                data: {
                    data: data
                }
            }).done(function (message) {
               // alert('Оборудование успешно добавлено в базу данных');
                //$('#ab-addResults').modal('hide');
                //$('#to-remove').detach().remove();
                $('#anchor').removeClass('bg-danger').addClass('bg-success').html( '<br>Оборудование успешно добавлено в базу данных<br><br>');
                // window.location.href = $site_url + 'addnews.html';
            }).fail(function (message) {
                alert('Ошибка!');
//                $('#ab-addResults').modal('hide');
  //              $('#to-remove').detach().remove();
                $('#anchor').addClass('bg-danger').html(message.responseText);
//                alert(message.responseText);
            });


        });

    });

})(jQuery);