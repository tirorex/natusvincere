
(function ($) {

    $(function () {

        $('input[type="radio"]').on('click', function (event) { //eslint-disable-line

            let name = this.name;
            let value = this.value;

            let othername = 'side2';
            let othervalue = '1';

            if (name == 'side2') {
                othername = 'side1';
            }
            if (value == '1') {
                othervalue = '2';
            }

            $("input[name='"+othername+"'][value='"+othervalue+"']").prop('checked',true);
            $("input[name='"+othername+"'][value='"+othervalue+"']").attr('checked',true);

        });

        $('#ab-send-results').on('click', function (event) {
            event.preventDefault();

            function checkBalls(n) {
                if (n >= 0 && n <= 10) {
                    return true;
                }
                    return false;
            }

            let side1 = $('input[name="side1"]:checked').val();
            let side2 = $('input[name="side2"]:checked').val();
            let villain = $('select[name="villain"]').val();
            let villainName = $('select[name="villain"] option:selected').text();
            let side1_score = $('select[name="side1_score"]').val();
            let side2_score = $('select[name="side2_score"]').val();
            let side1_miss = $('select[name="side1_miss"]').val();
            let side2_miss = $('select[name="side2_miss"]').val();

            let $error = '';

            if (!side1 || !side2) {
                $error += 'Выберите сторону<br>';
            }
            if (!villain) {
                $error += 'Укажите соперника<br>';
            }
            if (!side1_score || !side2_score || !side1_miss || !side2_miss) {
                $error += 'Не задано число забитых/пропущенных мячей<br>';
            }

            if (!checkBalls(side1_score) || !checkBalls(side2_score)
                || !checkBalls(side1_miss) || !checkBalls(side2_miss)) {
                $error += 'Не задано число забитых/пропущенных мячей<br>';
            }

            if ( (side1_score == side1_miss) || (side2_score == side2_miss)) {
                $error += 'Ничья не разрешена регламентом<br>';
            }

            if ( (side1_score >= 10 || side1_miss >= 10 ) &&
                 (side2_score >= 10 || side2_miss >= 10 )) {
               //null
            }else {
                $error += 'Не достигнута отметка в 10 мячей<br>';
            }

            if ($error) {
                $('#anchor').addClass('bg-danger').html($error);
                return false;
            }

            $('#anchor').removeClass('bg-danger').html('');

            $('#ab-addResults').modal('show');
            $('#for-results').empty().html('Ваш соперник <b>' + villainName + '</b><br>Первый матч ' + side1_score + ':' + side1_miss
                + '<br>Второй матч ' + side2_score + ':' + side2_miss
            );

        });

        $('#ab-confirm-results').one('click', function (event) {

            event.preventDefault();

            let side1 = $('input[name="side1"]:checked').val();
            let side2 = $('input[name="side2"]:checked').val();
            let villain = $('select[name="villain"]').val();
            let side1_score = $('select[name="side1_score"]').val();
            let side2_score = $('select[name="side2_score"]').val();
            let side1_miss = $('select[name="side1_miss"]').val();
            let side2_miss = $('select[name="side2_miss"]').val();

            let data = [];
            data[0] = 'addResults';
            data[1] = side1;
            data[2] = side2;
            data[3] = villain;
            data[4] = side1_score;
            data[5] = side1_miss;
            data[6] = side2_score;
            data[7] = side2_miss;
            data[8] = '';

            $.ajax({
                type: 'POST',
                url: $php_url + 'ajax.php',
                data: {
                    data: data
                }
            }).done(function (message) {
                $('#ab-addResults').modal('hide');
                $('#to-remove').detach().remove();
                $('#anchor').addClass('bg-success').html(message + '<br><br>После подтверждения результата поединка вашим соперником будут начислены рейтинговые очки');
                // window.location.href = $site_url + 'addnews.html';
            }).fail(function (message) {
                $('#ab-addResults').modal('hide');
                $('#to-remove').detach().remove();
                $('#anchor').addClass('bg-danger').html(message.responseText);
//                alert(message.responseText);
            });


        });

    });

})(jQuery);