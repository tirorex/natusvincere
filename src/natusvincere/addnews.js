//Указать глобальные переменный для eslint
/* global $php_url: false, $site_url:false */

(function ($) {

    $(function () {

        $('#ab-send').on('click', function () {

            let news = $('#ab-news').prop('value');
            let email = $('#ab-email');

            let sendMail = 0;
            if (email.prop('checked')) {
                sendMail = 1;
            }

            let header = $('#ab-header').prop('value');
            let data = [];

            data[0] = 'addNews';
            data[1] = header;
            data[2] = news;
            data[3] = sendMail;

            $.ajax({
                type: 'POST',
                url: $php_url + 'ajax-admin.php',
                data: {
                    data: data
                }
            }).done(function (message) {
             //   alert(message);
                window.location.href = $site_url + 'addnews.html';
            }).fail(function (message) {
              //  alert(message.responseText);
            });
        });
    });

})(jQuery);