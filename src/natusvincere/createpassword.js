(function ($) {

    $(function () {

        $(function () {
           //
        });

        $('#ab-send').on('click', function (event) {
            event.preventDefault();

            let serialize = $('form').serialize();

            let p1 = $('form [name="password1"]').val();
            let p2 = $('form [name="password2"]').val();

            if (p1 != p2) {
                 $('<div class="alert alert-danger my-3">').text('Пароли не совпадают')
                    .appendTo( $('.container .row div').eq(0))
                    .delay(2000).hide(300, function () {
                        $(this).detach().remove();
                    });
                 return false;
            }

            $.ajax({
                type: "POST",
                url: $php_url + 'new_user_check_email.php',
                data:  serialize
            }).done(function (message) {
                document.cookie = $cookie_name + '=' + message + '; path=' + $cookie_path + ';';
                window.location.href=$main_url;
            }).fail(function (message) {
               alert( message.responseText);
            });

        });

    });


})(jQuery);

