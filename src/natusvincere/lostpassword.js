(function ($) {

    $(function () {

              $('#ab-send').on('click', function (event) {
            event.preventDefault();

            let hash = $('#hash').val();
            let serialize = $('form').serialize();

            if (hash) {
                let password1 = $('form [name="password1"]').val();
                let password2 = $('form [name="password2"]').val();
                if (password1 != password2 || password1.length <= 6) {
                    $('<div class="alert alert-danger my-3">').text('Пароли должны совпадать и содержать не менее 6 символов')
                        .appendTo( $('.container .row div').eq(0))
                        .delay(2000).hide(300, function () {
                        $(this).detach().remove();
                    });
                    return false;
                }

                $.ajax({
                    /* eslint-disable */
                    type: "POST",
                    /* eslint-enable */
                    url: $php_url + 'lostpassword.php',
                    data:  serialize
                    /* eslint-disable no-unused-vars */
                }).done(function (message) {
                    /* eslint-enable */
                    alert('Пароль успешно сменен');
                    window.location.href=$main_url;
                }).fail(function (message) {
                    alert( message.responseText);
                });

                return;
            }


            let email = $('form [name="email"]').val();
            let surname = $('form [name="surname"]').val();

            if (email.length <= 5 || surname.length <= 5) {
                 $('<div class="alert alert-danger my-3">').text('Заполните поля')
                    .appendTo( $('.container .row div').eq(0))
                    .delay(2000).hide(300, function () {
                        $(this).detach().remove();
                    });
                 return false;
            }


            $.ajax({
                /* eslint-disable quotes */
                type: "POST",
                /* eslint-enable */
                url: $php_url + 'lostpassword.php',
                data:  serialize
            }).done(function (message) {
                $('<div class="alert alert-success my-3">').text(message)
                    .appendTo( $('.container .row div').eq(0));
            }).fail(function (message) {
               alert( message.responseText);
            });

        });

    });


})(jQuery);

