
(function ($) {

    $(function () {

        $('.dropdown-menu a').on('click', function (event) { //eslint-disable-line no-unused-vars

            let item = $(this);
            var action = '';
            let checked = $('table input:checked');

            $('#res').detach().remove();
            $('#res-button').detach().remove();

            if (checked.length == 0) {
                $('<div class="alert alert-danger">').text('Заявки не выбраны')
                    .prependTo('.content').delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });
                return false;
            }


            var res = $('<div id="res" class="mt-3">').appendTo('.content');

            if (item.attr('name') == 'accept') {
                action = 'Подтверждение';
                res.text('Вы выбрали ПОДТВЕРДИТЬ заявки.');
                res.addClass("alert alert-success");
            }
            if (item.attr('name') == 'decline') {
                action = 'Отказ';
                res.text('Вы выбрали ОТВЕРГНУТЬ заявки.');
                res.addClass("alert alert-danger");
            }

            $('<button id="res-button" type="button" data-toggle="modal" data-target="#ab-confirm" class="btn btn-primary"></button>').text('Исполнить').appendTo('.content');

            $('#ab-confirm').on('show.bs.modal',function () {

                let checked = $('table input:checked');
                var ids = "";

                checked.each(function () {
                    ids += this.name + ' ';
                });

                ids.trim();

                $('#ab-yes').one('click', function () {


                    var data = [];
                    data[0] = 'changeUserVerified';
                    if (action == 'Подтверждение') {
                        data[1] = 1;
                    } else {
                        data[1] = 4;
                    }

                    data[2] = ids.split(' ');

                    $.ajax({
                        type: "POST",
                        url: $php_url + 'ajax-admin.php',
                        data: {
                            data: data
                        }
                    }).done(function (message) { //eslint-disable-line no-unused-vars
                        window.location.href = $site_url + 'requests.html';
                    }).fail(function (message) {
                        $('#ab-confirm').modal('hide');
                        alert(message.responseText);
                    });
                });

                $('.ab-res').html( 'Подтвердите действие для ID ' + ids + ': <b>' + action + '</b>');
            });

        });
    });

})(jQuery);