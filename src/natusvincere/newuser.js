
(function ($) {

    $(function () {

        $('#ab-submit').on('click', function (event) {

            event.preventDefault();

            let name = $('input[name="name"]').val();
            let surname = $('input[name="surname"]').val();
            let middle_name = $('input[name="middle_name"]').val();

            if (typeof name === 'undefined' || typeof surname === 'undefined' || typeof middle_name === 'undefined'
                || name === "" || surname === "" || middle_name === "") {

                $('<div class="alert alert-danger col-9">').text('Пожалуйста, представтесь')
                    .prependTo('.anchor').delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });

                return false;
            }

            let data = $('form').serializeArray();
            $.ajax({
                type: "POST",
                url: $php_url + 'newuser.php',
                data: data
            }).done(function (message) { //eslint-disable-line no-unused-vars
                window.location.href = $site_url + 'newuser.html';
            }).fail(function (message) {
                $('<div class="alert alert-danger col-9">').text(message.responseText)
                    .prependTo('.anchor').delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });
            });
        });
    });

})(jQuery);