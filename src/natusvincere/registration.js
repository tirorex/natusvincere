(function ($) {

    $(function () {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        // Включить регистрацию при подтверждении пользователем условий регистрации
        $('#ab-agree').on('change', function () {

            if ( $(this).prop('checked')) {
                $('#ab-send').removeClass('disabled');
                $('#ab-warning').slideUp(500);
                $('#ab-email').removeClass('invisible');
            }
            else {
                $('#ab-send').addClass('disabled');
                $('#ab-warning').slideDown(500);
                $('#ab-email').addClass('invisible');

            }
        });

        // Сбросить переключатель подтверждения условия регистрации и откключаем кнопку регистрации
        $('#registration').on('show.bs.modal', function () {
            $('#ab-agree').prop('checked', false);
            $('#ab-send').addClass('disabled');
            $('#ab-warning').show();
            $('#ab-email').addClass('invisible');
            $('#registration .form-control').val('');
        });



        $('#ab-send').on('click', function () {

            let email = $('#registration .form-control').val();

            if (typeof email === 'undefined' || email === "") { //eslint-disable-line

                $('<div class="alert alert-danger">').text('Заполните поле email').prependTo('#ab-email')
                    .delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });
                return false;
            }

            $.ajax({
                //eslint-disable-next-line
                type: "POST",
                url: $php_url + 'new_user_send_email.php',
                data: {
                    'email': email
                }
            }).done(function (message) {
                $('#registration').modal('hide');
                $('.container .jumbotron').detach().remove();
                $('<div class="alert alert-success col" style="font-size:3em">').text('На ваш электронный адрес отправлено письмо' + message).appendTo($('.container .row'));
            }).fail(function(message){
                $('#registration').modal('hide');
                $('.container .jumbotron').detach().remove();
                $('<div class="alert alert-danger col" style="font-size:3em">').text('Ой, что-то пошло не так ' + message.responseText).appendTo($('.container .row'));
            });
        });

        $('#login').on('show.bs.modal', function () {
            $('#login input').val('');
        });

        $('#ab-login').on('click', function(event){

            let email = $('.f input[name="email"]').val();
            let password = $('.f input[name="password"]').val();

            if (typeof email === 'undefined' || typeof password === 'undefined'
                || email === "" || password === "") {

                $('<div class="alert alert-danger col-9">').text('Заполните поля email и пароль')
                    .prependTo('.f .d-flex').delay(2000).hide(300, function () {
                        $(this).detach().remove();
                     });

                return false;
            }

            event.preventDefault();

            let data = $('.f').serializeArray();
            $.ajax({
                type: "POST",
                url: $php_url + 'auth.php',
                data: data
            }).done(function (message) {
                document.cookie = $cookie_name + '=' + message + '; path=' + $cookie_path + ';';
                window.location.href = $main_url;
            }).fail(function (message) {
                $('<div class="alert alert-danger col-9">').text(message.responseText)
                    .prependTo('.f .d-flex').delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });
            });

        });
    });


})(jQuery);