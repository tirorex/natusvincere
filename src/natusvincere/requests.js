
(function ($) {

    $(function () {

        $('.dropdown-menu a').on('click', function (event) { //eslint-disable-line no-unused-vars

            let item = $(this);
            var action = '';
            let checked = $('table input:checked');

            $('#res').detach().remove();
            $('#res-button').detach().remove();
            $('#ab-default-priv').detach().remove();


            if (checked.length == 0) {
                $('<div class="alert alert-danger">').text('Заявки не выбраны')
                    .prependTo('.content').delay(2000).hide(300, function () {
                    $(this).detach().remove();
                });
                return false;
            }


            var res = $('<div id="res" class="mt-3">').appendTo('.content');

            if (item.attr('name') == 'accept') {
                action = 'Подтверждение';
                res.text('Вы выбрали ПОДТВЕРДИТЬ заявки.');
                res.addClass("alert alert-success");
                var defaultPrivs = $('<div id="ab-default-priv">');
                var div1 = $('<div class="form-check form-check-inline">');
                var div2 = $('<label class="form-check-label">Игрок</label>');
                var div3 = $('<input class="form-check-input" type="radio" name="defpriv" value="2">');

                div3.prependTo(div2);
                div2.appendTo(div1);
                div1.appendTo(defaultPrivs);

                var div11 = $('<div class="form-check form-check-inline">');
                var div22 = $('<label class="form-check-label">Наблюдатель</label>');
                var div33 = $('<input class="form-check-input" checked type="radio" name="defpriv" value="3">');

                div33.prependTo(div22);
                div22.appendTo(div11);
                div11.appendTo(defaultPrivs);

                defaultPrivs.appendTo('.content');
            }
            if (item.attr('name') == 'decline') {
                action = 'Отказ';
                res.text('Вы выбрали ОТВЕРГНУТЬ заявки.');
                res.addClass("alert alert-danger");
            }

            $('<button id="res-button" type="button" data-toggle="modal" data-target="#ab-confirm" class="btn btn-primary"></button>').text('Исполнить').appendTo('.content');

            $('#ab-confirm').on('show.bs.modal',function () {

                let checked = $('table input:checked');
                var ids = "";

                let priv = $('input[name="defpriv"]:checked').prop('value');

                checked.each(function () {
                    ids += this.name + ' ';
                });

                ids.trim();

                $('#ab-yes').one('click', function () {


                    var data = [];
                    data[0] = 'changeUserVerified';
                    if (action == 'Подтверждение') {
                        data[1] = 1;
                    } else {
                        data[1] = 4;
                    }

                    data[2] = ids.split(' ');

                    data[3] = priv;

                    $.ajax({
                        type: "POST",
                        url: $php_url + 'ajax-admin.php',
                        data: {
                            data: data
                        }
                    }).done(function (message) { //eslint-disable-line no-unused-vars
                        window.location.href = $site_url + 'requests.html';
                    }).fail(function (message) {
                        $('#ab-confirm').modal('hide');
                        alert(message.responseText);
                    });
                });

                $('.ab-res').html( 'Подтвердите действие для ID ' + ids + ': <b>' + action + '</b>');
            });

        });
    });

})(jQuery);